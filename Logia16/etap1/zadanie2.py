# -*- coding: utf-8 -*-

from skoki import *

""" Zadanie 4: Piramida
    Treść:
        Napisz jednoparametrową procedurę/funkcję piramida,
        po wywołaniu której na środku ekranu powstanie
        rysunek piramidy takiej, jak poniżej. Parametr określa
        liczbę poziomów piramidy, która jest także liczbą
        elementów w podstawie piramidy. Parametr może
        przyjmować wartości od 1 do 10. Wysokość każdego
        poziomu jest stała i wynosi 48.
"""

# Stałe
wysokosc_poziomu = 96
ilosc_pikseli_w_wysokosci = 12
szerokosc_pedzla = wysokosc_poziomu / ilosc_pikseli_w_wysokosci
szerokosc_poziomu = szerokosc_pedzla * 14
szerokosc_ramki = szerokosc_pedzla * 4

maksymalna_wartosc = 10
minimalna_wartosc = 1

def rysuj_prostokat (a, b):
    turtle.begin_fill()
    for i in range (0, 2):
        turtle.forward(a)
        turtle.right(90)
        turtle.forward(b)
        turtle.right(90)
    turtle.end_fill()

def idz (ile, kierunek = None):
    rysuj_prostokat(szerokosc_pedzla * ile, szerokosc_pedzla)

    turtle.penup()
    turtle.forward(szerokosc_pedzla * ile)
    turtle.pendown()

    if kierunek == None:
        turtle.penup()
        turtle.backward(szerokosc_pedzla * ile)
        turtle.pendown()
    elif kierunek == 'l':
        turtle.penup()
        turtle.backward(szerokosc_pedzla)
        turtle.pendown()

        turtle.left(90)
    else:
        turtle.right(90)

def rysuj_element_piramidy (poziom):
    """ Funkcja rysuje element mozaiki w zależności od poziomu
        Zawiera stałą wysokość 48 pikseli
    """

    turtle.fillcolor("#c88048")
    rysuj_prostokat(szerokosc_ramki + szerokosc_poziomu * poziom + szerokosc_pedzla,
                    wysokosc_poziomu)
    skocz_o(szerokosc_pedzla, szerokosc_pedzla)
    turtle.fillcolor("#ffff00")
    rysuj_prostokat(szerokosc_poziomu * poziom + 3 * szerokosc_pedzla,
                    wysokosc_poziomu - 2 * szerokosc_pedzla)
    skocz_o(szerokosc_pedzla, 9*szerokosc_pedzla)

    turtle.fillcolor("#c88048")
    turtle.left(90)
    for i in range(0, poziom):
        idz(8, 'p')
        idz(7, 'p')
        idz(5, 'p')
        idz(3, 'p')
        idz(3, 'l')
        idz(2, 'l')
        idz(4, 'l')
        idz(10, 'l')
        idz(4, 'l')
        idz(2, 'l')
        idz(2, 'p')
        idz(3, 'p')
        idz(5, 'p')
        idz(7, 'p')
        idz(8)

        skocz_o (szerokosc_poziomu, 0)

def piramida (poziomy):
    """ Rysuje całość Piramidy
    """
    if poziomy < minimalna_wartosc or poziomy > maksymalna_wartosc:
        print ("Złe parametry. Liczba musi być z przedziału [%d..%d]" % (minimalna_wartosc, maksymalna_wartosc))

    # turtle.pensize(szerokosc_pedzla)
    turtle.pencolor("#c88048")
    turtle.speed(5000)

    max_szerokosc = szerokosc_ramki + szerokosc_poziomu * poziomy + szerokosc_pedzla
    max_wysokosc = wysokosc_poziomu * (poziomy-1)

    skocz_o (-max_szerokosc/2, max_wysokosc/2)

    for poziom in range (poziomy, 0, -1):
        turtle.setheading(0)
        local_home_position = turtle.position()
        rysuj_element_piramidy(poziom)
        skocz_do(local_home_position)
        skocz_o((szerokosc_poziomu)/2, -wysokosc_poziomu + szerokosc_pedzla)

    turtle.hideturtle()

piramida (5)
input()
