# -*- coding: utf-8 -*-

""" Zadanie 4: irs
    Treść:
        Napisz jednoparametrową funkcję irs, której
        wynikiem jest n-ta w porządku rosnącym dodatnia
        liczba całkowita mająca tę cechę, że iloczyn cyfr
        tej liczby jest równy sumie cyfr tej liczby. Parametr n
        może przyjmować wartości od 1 do 98.
"""

def suma (cyfry):
    """ Funkcja zwracająca sumę cyfr w tablicy
    """
    suma = 0
    for cyfra in cyfry:
        suma = suma + cyfra

    return suma

def iloczyn (cyfry):
    """ Funkcja zwracająca iloczyn liczb w tablicy
    """
    iloczyn = 1
    for cyfra in cyfry:
        iloczyn = iloczyn * cyfra

    return iloczyn

def wez_cyfry (liczba):
    """ Funkcja rozkładająca liczbę na tablicę cyfr
    """
    cyfry = [] # Deklaracja pustej tablicy
    while liczba > 0: # Dopóki liczba jest niezerowa (będziemy odejmować)
        cyfra = liczba % 10 # Jedności: ostatnia liczba po prawo (reszta z dz. przez 10)
        cyfry.append(cyfra) # Wrzuć cyfrę do listy cyfr
        liczba = int((liczba - cyfra)/10) # Przesuń liczbę w prawo (usuń jedności, podziel przez 10)

    # Zwróć listę cyfr
    return cyfry

def irs (liczba):
    """ Funkcja irs: przeszukuje n-tą liczbę, której suma cyfr jest równa
        iloczynowi cyfr.

        1. Wyzeruj licznik wystąpień, rozpocznij liczenie od 1
        2. Dla każdej liczby naturalnej N:
            1. Jeżeli licznik wskazuje podaną wartość przez użytkownika,
                Zwróć aktualną liczbę
            1. Sprawdź, czy suma(cyfry(n)) = iloczyn(cyfry(n))
                Tak: Zwiększ licznik
                Nie: Kontynuuj porównywanie
    """

    if liczba < 1 or liczba > 98:
        print ("Złe parametry. Liczba musi być z przedziału [1..98]")

    # Część 1:
    ilosc = 0 # Wyzeruj licznik
    indeks = 1

    # Część 2:
    while True:
        cyfry = wez_cyfry(indeks) # Pobierz cyfry liczby
        if suma (cyfry) == iloczyn(cyfry): # Sprawdź czy suma = iloczyn
            ilosc = ilosc + 1 # Powiększ licznik wystąpień o 1
        if (ilosc == liczba): # Jeżeli otrzymano n-tą liczbę
            return indeks # Zwróć tę liczbę
        indeks = indeks + 1 # przejdź do kolejnej liczby

# Testy (zgodnie z przykładami w zadaniu)
if irs(5) == 5:
    print ("irs(5): Passed (%d)" % irs(5))
else:
    print ("irs(5): ", irs(5))

if irs(11) == 123:
    print ("irs(11): Passed (%d)" % irs(11))
else:
    print ("irs(11): ", irs(11))

if  irs(69) == 111126:
    print ("irs(69): Passed (%d)" % irs(69))
else:
    print ("irs(69): ", irs(69))
