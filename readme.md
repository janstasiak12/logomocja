# Logia

Zawartość:

1. [O projekcie](#markdown-header-o-projekcie)
1. [Zasady](#markdown-header-zasady)
1. [Komendy git](#markdown-header-komendy-git)
1. [Autorzy](#markdown-header-autorzy)

## O projekcie

W tym repozytorium znajdują się rozwiązania zadań z konkursu informatycznego Logia dla gimnazjów, organizowanego przez Ośrodek Edukacji Informatycznej i Zastosowań Komputerów w Warszawie.

Informacje o konkursie dostępne są na stronie internetowej: [logia.oeiizk.waw.pl](http://logia.oeiizk.waw.pl/). Repozytorum jest pod systemem kontroli wersji `git`. W zakładce '[Komendy git](#markdown-header-komendy-git)' zawarte są podstawowe informacje o obsłudze systemu `git`. Samouczek znajduje się pod adresem: [try.github.io](http://try.github.io).

## Zasady

Implementacje zadań wykonane są w języku Python w wersji 3.5 z wykorzystaniem bibliotek `turtle`. Więcej informacji znajduje się pod adresami:
1. [python.oeiizk.edu.pl](http://python.oeiizk.edu.pl/) - bieda poradnik,
1. [docs.python.org/3.5](https://docs.python.org/3.5/) oficjalna dokumentacja (ang).

Każdy folder repozytorium zawiera rozwiązania zadań z poszczególnych edycji konkursu. W każdym folderze edycji znajdują się treści zadań oraz kody źródłowe programów dla kolejnych etapów.

Przykładowe drzewo katalogów:
```
.
├── Logia16
│   ├── etap1
│   │   ├── L161zad.pdf
│   │   ├── zadanie1.py
│   │   ├── zadanie2.py
│   │   ├── zadanie3.py
│   │   └── zadanie4.py
│   ├── etap2
│   └── etap3
└── readme.md
```

## Komendy git

Dla ułatwienia współpracy korzystamy z jednej gałęzi (master). Git jest używany w celu synchronizacji zawartości projektu. Staraj się jak najczęściej sprawdzać, czy pracujesz na najnowszej rewizji. Tutorial w języku angielskim znajduje się pod adresem [www.youtube.com/watch?v=r63f51ce84A](https://www.youtube.com/watch?v=r63f51ce84A).
Git jest rozproszonym systemem kontroli wersji, to znaczy, że na każdym z komputerów zawierających repozytorium znajduje się dokładna kopia tego, co jest na serwerze (w zasadzie nie ma serwera, tylko umownie określamy, gdzie umieszczamy nasz kod - w tym wypadku na serwisie `BitBucket`). Przed wrzuceniem zmian na serwer, musisz wrzucić zmiany do twojego lokalnego repozytorium.

Poniżej zestawione są najbardziej podstawowe komendy `git`:

#### Wyświetlanie informacji

```
$ git status          # Wyświetla aktualnie zmienione pliki
$ git log             # Wyświetla log zmian z commitami
$ git log --graph     # Zmiany w commitach w formie drzewa            
```

#### Pobieranie zmian z serwera:

```
$ git pull            # Nanosi zmiany z serwera na twoje lokalne repozytorium
$ git pull --rebase   # !!!! Kopiuje lokalne zmiany do schowka, pobiera zawartość z serwera, po czym nanosi zmiany lokalne (ze schowka) - UŻYWAJ TEGO
```

#### Wrzucanie zmian na serwer

```
$ git add nazwa_pliku             # Dodaj plik do `staging area` - miejsca pod kontrolą (przed każdym commitem)
$ git add .                       # Dodaj wszystkie pliki do `staging area`
$ git commit -m "Tytul zmian"     # Wrzuć zmiany do lokalnego repozytorium
$ git push                        # Pchaj zmiany na serwer
```

## Autorzy

1. Wojtek Ossowski
1. Jan Stasiak
